# Firebase Setup

This project is based on the Firebase Hosting, Database, and Cloud Function.

## Create Project

If you haven't already create a Firebase project, go to https://console.firebase.google.com/ to create a new Firebase project.

Please use the same project for both Android and Web parts as they are sharing databases.


## Add Project

In order to add your project into the source code, go to Firebase Console, select your project and click "Add another app" from the screen.

Follow the steps to generate config file and replace with the one in the source code.