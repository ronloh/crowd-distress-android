package wenheloh.crowddistress;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import wenheloh.crowddistress.helpers.AddedContactsArrayAdapter;
import wenheloh.crowddistress.helpers.PendingContactsArrayAdapter;
import wenheloh.crowddistress.helpers.RequestContactsArrayAdapter;
import wenheloh.crowddistress.helpers.SessionManager;
import wenheloh.crowddistress.model.Contacts;

public class AddContactActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    private AlertDialog.Builder builder;
    private Context context;
    private FirebaseUser currentUser;
    private String targetEmail;
    private String targetUid;
    private String currentUserEmail;
    private ListView lv_request;
    private ListView lv_pending;
    private ListView lv_added;
    private ArrayList<Contacts> requestList;
    private ArrayList<Contacts> pendingList;
    private ArrayList<Contacts> addedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        context = this;

        lv_request = findViewById(R.id.lv_request);
        lv_pending = findViewById(R.id.lv_pending);
        lv_added = findViewById(R.id.lv_added);

        TextView tvHdr_request = new TextView(context);
        TextView tvHdr_pending = new TextView(context);
        TextView tvHdr_added = new TextView(context);

        tvHdr_request.setText("Request Received");
        tvHdr_request.setTextColor(getResources().getColor(R.color.color292929));
        tvHdr_request.setPadding(20, 50, 10, 5);

        tvHdr_pending.setText("Pending Request");
        tvHdr_pending.setTextColor(getResources().getColor(R.color.color292929));
        tvHdr_pending.setPadding(20, 50, 10, 5);

        tvHdr_added.setText("Added Contact");
        tvHdr_added.setTextColor(getResources().getColor(R.color.color292929));
        tvHdr_added.setPadding(20, 50, 10, 5);

        lv_request.addHeaderView(tvHdr_request);
        lv_pending.addHeaderView(tvHdr_pending);
        lv_added.addHeaderView(tvHdr_added);

        initializeRequestRecyclerView();
        initializePendingRecyclerView();
        initializeAddedRecyclerView();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setupActionBar();

        sessionManager = new SessionManager(this);

        // Setup OnClickListener for the buttons
        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                TextView tvTitle;
                final EditText input;

                switch (view.getId()) {

                    case R.id.btn_addContact:

                        // Setup AlertDialog
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.CustomAlertDialogWithEditText));
                        } else {
                            builder = new AlertDialog.Builder(context);
                        }

                        input = new EditText(context);
                        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        input.setMaxLines(1);
                        ColorStateList ctl = new ColorStateList(
                                new int[][]{
                                        new int[]{android.R.attr.state_focused},
                                        new int[]{}
                                },
                                new int[]{
                                        context.getResources().getColor(R.color.colorPrimaryDark),
                                        context.getResources().getColor(R.color.colorAccent)
                                }
                        );

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            input.setBackgroundTintList(ctl);
                        }

                        tvTitle = new TextView(context);
                        tvTitle.setText("Contact's email: ");
                        tvTitle.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        tvTitle.setPadding(20, 50, 10, 5);

                        builder.setCustomTitle(tvTitle)
                                .setView(input)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Do nothing here, and override original behaviour at below.
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        dialog.dismiss();
                                    }
                                });

                        final AlertDialog dialog = builder.create();
                        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        dialog.show();

                        // Override the default behaviour of the Positive button
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String email = input.getText().toString();

                                if (isValidEmail(email)) {
                                    targetEmail = email;
                                    searchEmailAndAdd(dialog);
                                } else {
                                    Toast.makeText(AddContactActivity.this, "Please enter a valid email address.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        break;

                }
            }
        };

        findViewById(R.id.btn_addContact).setOnClickListener(listener);

    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void searchEmailAndAdd(final AlertDialog dialog) {

        targetEmail = targetEmail.replaceAll("[@.]", "_");
        currentUserEmail = currentUser.getEmail().replaceAll("[@.]", "_");

        if (targetEmail.equals(currentUserEmail)) {
            Toast.makeText(context, "Cannot add your own account as emergency contact.", Toast.LENGTH_SHORT).show();
            return;
        }

        final DatabaseReference searchRef = FirebaseDatabase.getInstance().getReference()
                .child("emails")
                .child(targetEmail);

        searchRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {  // IF user exists

                    // Check if the contact is already been added.
                    final Boolean[] alreadyAdded = {false};
                    final int[] count = {0};
                    targetUid = dataSnapshot.getValue().toString();

                    DatabaseReference ownAddedRef = FirebaseDatabase.getInstance().getReference()
                            .child("contacts")
                            .child(currentUserEmail)
                            .child("added");

                    ownAddedRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                                count[0]++;

                                Contacts contacts = snapshot.getValue(Contacts.class);
                                if (targetEmail.equals(contacts.getEmail())) {
                                    alreadyAdded[0] = true;
                                }

                            }

                            if (count[0] >= 10) {
                                Toast.makeText(AddContactActivity.this, "You have reached the max amount of emergency contacts.", Toast.LENGTH_SHORT).show();
                            } else if (!alreadyAdded[0]) {

                                // Check if already in pending list
                                final Boolean[] alreadyPending = {false};

                                DatabaseReference ownPendingRef = FirebaseDatabase.getInstance().getReference()
                                        .child("contacts")
                                        .child(currentUserEmail)
                                        .child("pending");

                                ownPendingRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                                            Contacts contacts = snapshot.getValue(Contacts.class);
                                            if (targetEmail.equals(contacts.getEmail())) {
                                                alreadyPending[0] = true;
                                            }

                                        }

                                        if(!alreadyPending[0]) {
                                            // Record this request in database
                                            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference()
                                                    .child("contacts")
                                                    .child(targetEmail)
                                                    .child("request");

                                            final String key = userRef.push().getKey();

                                            HashMap<String, Object> hmRequest = new HashMap<>();
                                            hmRequest.put("requestId", key);
                                            hmRequest.put("email", currentUserEmail);
                                            hmRequest.put("username", sessionManager.getName());
                                            hmRequest.put("uid", currentUser.getUid());
                                            hmRequest.put("imageUrl", sessionManager.getImageUrl());

                                            userRef.child(key).updateChildren(hmRequest);

                                            userRef.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    // Record in currentUser's node
                                                    recordInPendingNode(key, false);

                                                    // Send push notification
                                                    DatabaseReference tokenRef = FirebaseDatabase.getInstance().getReference()
                                                            .child("fcmTokens")
                                                            .child(targetUid);

                                                    tokenRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            String currentToken = dataSnapshot.getValue().toString();

                                                            FirebaseDatabase.getInstance().getReference()
                                                                    .child("requestPush")
                                                                    .child(currentToken)
                                                                    .setValue(true);

                                                            dialog.dismiss();
                                                            // Show dialog Request sent
                                                            buildAlertMessage("Request sent, please remind your contact to accept request.");
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {

                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                        } else {
                                            Toast.makeText(AddContactActivity.this, "Contact already in pending list.", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                            } else {
                                Toast.makeText(AddContactActivity.this, "You already added this contact.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                } else {
                    // Send email to invite
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                            .child("invitation");

                    ref.push().child(targetEmail).child(sessionManager.getName()).setValue("true");

                    // Record this request in database
                    DatabaseReference userRef = FirebaseDatabase.getInstance().getReference()
                            .child("contacts")
                            .child(targetEmail)
                            .child("request");

                    final String key = userRef.push().getKey();

                    final HashMap<String, Object> hmRequest = new HashMap<>();
                    hmRequest.put("requestId", key);
                    hmRequest.put("email", currentUserEmail);
                    hmRequest.put("username", sessionManager.getName());
                    hmRequest.put("uid", currentUser.getUid());
                    hmRequest.put("imageUrl", sessionManager.getImageUrl());

                    userRef.child(key).setValue(hmRequest);

                    // Record in currentUser's node
                    recordInPendingNode(key, true);

                    dialog.dismiss();

                    buildAlertMessage("An invitation email has been sent to your contact, please remind the person to download our application.");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void recordInPendingNode(final String key, Boolean isNewUser) {
        if (!isNewUser) {
            DatabaseReference getImageUrlRef = FirebaseDatabase.getInstance().getReference()
                    .child("users")
                    .child(targetUid);

            getImageUrlRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    DatabaseReference ownRef = FirebaseDatabase.getInstance().getReference()
                            .child("contacts")
                            .child(currentUserEmail)
                            .child("pending")
                            .child(key);

                    HashMap<String, Object> hmPending = new HashMap<>();
                    hmPending.put("requestId", key);
                    hmPending.put("email", targetEmail);
                    hmPending.put("uid", targetUid);
                    hmPending.put("imageUrl", dataSnapshot.child("imageUrl").getValue().toString());
                    hmPending.put("username", dataSnapshot.child("name").getValue().toString());

                    ownRef.setValue(hmPending);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            DatabaseReference ownRef = FirebaseDatabase.getInstance().getReference()
                    .child("contacts")
                    .child(currentUserEmail)
                    .child("pending")
                    .child(key);

            HashMap<String, Object> hmPending = new HashMap<>();
            hmPending.put("requestId", key);
            hmPending.put("email", targetEmail);

            ownRef.setValue(hmPending);
        }

    }

    private void initializeRequestRecyclerView() {
        currentUserEmail = currentUser.getEmail().replaceAll("[@.]", "_");

        requestList = new ArrayList<Contacts>();
        final RequestContactsArrayAdapter adapter = new RequestContactsArrayAdapter(context, requestList);
        lv_request.setAdapter(adapter);

        DatabaseReference contactRef = FirebaseDatabase.getInstance().getReference()
                .child("contacts")
                .child(currentUserEmail)
                .child("request");

        contactRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Contacts newContact = dataSnapshot.getValue(Contacts.class);
                adapter.add(newContact);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initializePendingRecyclerView() {
        currentUserEmail = currentUser.getEmail().replaceAll("[@.]", "_");

        pendingList = new ArrayList<Contacts>();
        final PendingContactsArrayAdapter adapter = new PendingContactsArrayAdapter(context, pendingList);
        lv_pending.setAdapter(adapter);

        DatabaseReference contactRef = FirebaseDatabase.getInstance().getReference()
                .child("contacts")
                .child(currentUserEmail)
                .child("pending");

        contactRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Contacts newContact = dataSnapshot.getValue(Contacts.class);
                adapter.add(newContact);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Toast.makeText(context, "Request deleted.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initializeAddedRecyclerView() {
        currentUserEmail = currentUser.getEmail().replaceAll("[@.]", "_");

        addedList = new ArrayList<Contacts>();
        final AddedContactsArrayAdapter adapter = new AddedContactsArrayAdapter(context, addedList);
        lv_added.setAdapter(adapter);

        DatabaseReference contactRef = FirebaseDatabase.getInstance().getReference()
                .child("contacts")
                .child(currentUserEmail)
                .child("added");

        contactRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Contacts newContact = dataSnapshot.getValue(Contacts.class);
                adapter.add(newContact);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Toast.makeText(context, "Contact deleted.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void buildAlertMessage(String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.CustomAlertDialog));
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
