package wenheloh.crowddistress;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseUserMetadata;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Arrays;
import java.util.HashMap;

import wenheloh.crowddistress.model.Contacts;
import wenheloh.crowddistress.helpers.SessionManager;
import wenheloh.crowddistress.model.User;

import static wenheloh.crowddistress.helpers.SessionManager.KEY_SETUP;
import static wenheloh.crowddistress.helpers.SessionManager.RC_SIGN_IN;

public class SignInActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    private HashMap<Object, Object> hmUser;
    private FirebaseAuth auth;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize Firebase
        FirebaseApp.initializeApp(this);
        auth = FirebaseAuth.getInstance();

        // Initialize SessionManager
        sessionManager = new SessionManager(this);

        startActivityForResult(
            AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(Arrays.asList(
                            new AuthUI.IdpConfig.EmailBuilder().build(),
                            new AuthUI.IdpConfig.GoogleBuilder().build()
                    ))
                    .setLogo(R.mipmap.ic_launcher)
                    .setIsSmartLockEnabled(false, true)
                    .build(),
            RC_SIGN_IN
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            final IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed in
            if (resultCode == RESULT_OK) {

                auth = FirebaseAuth.getInstance();
                currentUser = auth.getCurrentUser();

                // Check if user is newly registered
                FirebaseUserMetadata metadata = currentUser.getMetadata();
                if(metadata.getCreationTimestamp() == metadata.getLastSignInTimestamp()) {
                    newUserSetup();
                } else {
                    // TODO Testing for New user
                    if(currentUser.getUid().equals("abc79UUSPkbvkrZ89NUYaymOYq63")){

                        newUserSetup();

                        return;
                    }

                    FirebaseDatabase.getInstance().getReference()
                            .child("users")
                            .child(currentUser.getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            User user = dataSnapshot.getValue(User.class);

                            if(user != null) {

                                sessionManager.saveUser(
                                        user.getName(),
                                        user.getDate(),
                                        user.getGender(),
                                        user.getHeight(),
                                        user.getWeight(),
                                        user.getRace(),
                                        user.getImageUrl(),
                                        user.getPin(),
                                        user.getPhoneNo()
                                );

                                MainActivity.PinNumber = user.getPin();

                                // Get Device token for FCM Push Notification
                                String token = FirebaseInstanceId.getInstance().getToken();

                                if (token != null) {
                                    FirebaseDatabase.getInstance().getReference()
                                            .child("fcmTokens")
                                            .child(currentUser.getUid())
                                            .setValue(token);
                                }

                            } else {
                                Log.e("exception", "Fail to load user data");
                            }

                            Intent intent = new Intent(SignInActivity.this, MainActivity.class).putExtra("my_token", response.getIdpToken());
                            intent.putExtra("fromSigninActivity", true);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e("FireDatabase", databaseError.getMessage());
                        }
                    });
                }


            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    showToast("You must sign in first!");
                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("NotSignin", true);
                    startActivity(intent);
                    finish();
                    return;
                }

                if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    showToast("No internet connection");
                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("NotSignin", true);
                    startActivity(intent);
                    finish();
                    return;
                }

                showToast("Unknown error");
                Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("NotSignin", true);
                startActivity(intent);
                Log.e("LoginActivity", "Sign-in error: ", response.getError());
                finish();
            }
        }
    }

    private void newUserSetup() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(auth.getCurrentUser().getUid());

        HashMap<String, Object> addBool = new HashMap<>();
        addBool.put(KEY_SETUP, "1");
        addBool.put("isAlarmTriggered", "false");

        ref.updateChildren(addBool);

        // Save email to emails ref
        String userEmail = currentUser.getEmail();
        userEmail = userEmail.replaceAll("[@.]", "_");
        DatabaseReference emailRef = FirebaseDatabase.getInstance().getReference().child("emails").child(userEmail);
        emailRef.setValue(currentUser.getUid());

        Intent intent = new Intent(SignInActivity.this, SetupAccountActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void showToast(String text) {
        //View layout = findViewById(R.id.signin_layout);
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }
}
