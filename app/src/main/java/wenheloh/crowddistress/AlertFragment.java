package wenheloh.crowddistress;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class AlertFragment extends Fragment {


    private static final int CAMERA_PERMISSION_REQUEST_CODE = 2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_alert, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Button btnAlert = (Button) getView().findViewById(R.id.buttonAlert);
        //final Button btnTimer = (Button) getView().findViewById(R.id.buttonTimer);
        onPressHold(btnAlert);
        //setTimer(btnTimer);
    }

    @Override
    public void onStart() {
        super.onStart();

        if(!checkPermissionForCamera()) {

            try {
                requestPermissionForCamera();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    protected void onPressHold(final Button btnAlert) {
        if(btnAlert != null ) {
            btnAlert.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch(motionEvent.getAction()) {
                        //case MotionEvent.ACTION_BUTTON_PRESS:
                        case MotionEvent.ACTION_DOWN:
                            //btnTimer.setVisibility(View.GONE);

                            /* Convert DP into Pixels to setPadding
                            int paddingDp = 70;
                            float density = getContext().getResources().getDisplayMetrics().density;
                            int paddingPixel = (int)(paddingDp * density);
                            btnAlert.setPadding(paddingPixel,paddingPixel,paddingPixel,paddingPixel);

                            btnAlert.setScaleX(2);
                            btnAlert.setScaleY(2);
                            btnAlert.setBackgroundColor(Color.TRANSPARENT);
                            */

                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 101);

                            ScaleAnimation anim = new ScaleAnimation(1, 1.5f, 1, 1.5f,2, 0.5f, 2, 0.5f){
                                @Override
                                protected void applyTransformation(float interpolatedTime, Transformation t) {
                                    super.applyTransformation(interpolatedTime, t);
                                }
                            };

                            anim.setFillEnabled(true);
                            anim.setFillAfter(true);
                            anim.setDuration(500);
                            anim.setInterpolator(new AccelerateInterpolator());

                            // Convert DP into Pixels to setPadding
                            int paddingDp = 50;
                            float density = getContext().getResources().getDisplayMetrics().density;
                            int paddingPixel = (int)(paddingDp * density);
                            btnAlert.setPadding(paddingPixel,paddingPixel,paddingPixel,paddingPixel);

                            btnAlert.startAnimation(anim);
                            btnAlert.setBackgroundColor(Color.TRANSPARENT);


                            break;
                        case MotionEvent.ACTION_UP:
                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                                break;

                            ScaleAnimation anim2 = new ScaleAnimation(1.5f, 1, 1.5f, 1,2, 0.5f, 2, 0.5f){
                                @Override
                                protected void applyTransformation(float interpolatedTime, Transformation t) {
                                    super.applyTransformation(interpolatedTime, t);
                                }
                            };

                            anim2.setFillEnabled(true);
                            anim2.setFillAfter(true);
                            btnAlert.startAnimation(anim2);
                            btnAlert.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_corner_imgbtn));

                            // Convert DP into Pixels to setPadding
                            paddingDp = 20;
                            density = getContext().getResources().getDisplayMetrics().density;
                            paddingPixel = (int)(paddingDp * density);
                            btnAlert.setPadding(paddingPixel,paddingPixel,paddingPixel,paddingPixel);

                            //btnTimer.setVisibility(View.VISIBLE);

                            Intent intent = new Intent(getContext(), PinEntryActivity.class);
                            startActivity(intent);
                            break;
                        default:
                           // view.startAnimation(translate);
                            break;
                    }
                    return true;
                }
            });
        }
    }

    private boolean checkPermissionForCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    private void requestPermissionForCamera() throws Exception {
        try {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA},
                    CAMERA_PERMISSION_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
