package wenheloh.crowddistress;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import wenheloh.crowddistress.helpers.SessionManager;
import wenheloh.crowddistress.services.UploadImageService;

import static wenheloh.crowddistress.helpers.SessionManager.ACTION.ACTION_PROFILE_UPLOAD;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_BIRTH;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_DOWNLOAD_IMAGEURI;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_GENDER;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_HEIGHT;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_NAME;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_PHONE;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_RACE;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_WEIGHT;
import static wenheloh.crowddistress.helpers.SessionManager.PICK_IMAGE_REQUEST;

public class AccountFragment extends Fragment {

    private Intent intent;
    private Context context;
    SessionManager sessionManager;
    private HashMap<String, Object> hmUser;
    private DatabaseReference databaseRef;
    private static final int WRITE_STORAGE_PERMISSION_REQUEST_CODE = 4;
    private FirebaseStorage storage;
    private FirebaseUser currentUser;

    private final String TAG = "AccountFragment";

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity().getApplicationContext();
        sessionManager = new SessionManager(context);
        hmUser = sessionManager.getUser();
        storage = FirebaseStorage.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        // Initialize Firebase Realtime database Listener
        if (currentUser != null) {

            databaseRef = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
            databaseRef.keepSynced(true);

        }

        if (!checkPermissionForWriteExtertalStorage()) {
            //Toast.makeText(context, "Permission not granted", Toast.LENGTH_LONG).show();
            try {
                requestPermissionForWriteExtertalStorage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View.OnClickListener listener = new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {

                TextView tvTitle;
                final EditText input;
                AlertDialog.Builder builder;

                switch (view.getId()) {
                    case R.id.cardViewName:

                        if (context != null) {
                            // Setup AlertDialog
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.CustomAlertDialogWithEditText));
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                            }

                            input = new EditText(context);
                            input.setText(hmUser.get(KEY_NAME) == null ? null : hmUser.get(KEY_NAME).toString());
                            input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                            input.setMaxLines(1);
                            ColorStateList ctl = new ColorStateList(
                                    new int[][]{
                                            new int[]{android.R.attr.state_focused},
                                            new int[]{}
                                    },
                                    new int[]{
                                            context.getResources().getColor(R.color.colorPrimaryDark),
                                            context.getResources().getColor(R.color.colorAccent)
                                    }
                            );

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                input.setBackgroundTintList(ctl);
                            }

                            tvTitle = new TextView(context);
                            tvTitle.setText("Name");
                            tvTitle.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tvTitle.setPadding(20, 50, 10, 5);

                            builder.setCustomTitle(tvTitle)
                                    .setView(input)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            SessionManager mSessionManager = new SessionManager(context);
                                            mSessionManager.setName(input.getText().toString());
                                            updateProfileInfo();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    });

                            AlertDialog alertDialog = builder.create();
                            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                            alertDialog.show();
                        }

                        break;

                    case R.id.cardViewPhoneNo:

                        if (context != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.CustomAlertDialogWithEditText));
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                            }

                            input = new EditText(context);
                            input.setText(hmUser.get(KEY_PHONE) == null ? null : hmUser.get(KEY_PHONE).toString());
                            input.setInputType(InputType.TYPE_CLASS_PHONE);
                            input.setMaxLines(1);
                            ColorStateList ctl = new ColorStateList(
                                    new int[][]{
                                            new int[]{android.R.attr.state_focused},
                                            new int[]{}
                                    },
                                    new int[]{
                                            context.getResources().getColor(R.color.colorPrimaryDark),
                                            context.getResources().getColor(R.color.colorAccent)
                                    }
                            );
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                input.setBackgroundTintList(ctl);
                            }

                            tvTitle = new TextView(context);
                            tvTitle.setText("Phone No.");
                            tvTitle.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tvTitle.setPadding(20, 50, 10, 5);

                            builder.setCustomTitle(tvTitle)
                                    .setView(input)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            SessionManager mSessionManager = new SessionManager(context);
                                            mSessionManager.setPhone(input.getText().toString());
                                            updateProfileInfo();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    });

                            AlertDialog alertDialog = builder.create();
                            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                            alertDialog.show();
                        }

                        break;

                    case R.id.cardViewBirthday:

                        if (context != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light_Dialog));
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                            }

                            final DatePicker picker = new DatePicker(new ContextThemeWrapper(getActivity(), R.style.CustomPickerTheme));
                            //picker.setCalendarViewShown(true);

                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            Date date = null;
                            try {
                                date = simpleDateFormat.parse(hmUser.get(KEY_BIRTH) == null ? "01/01/1970" : hmUser.get(KEY_BIRTH).toString());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Calendar calendar = simpleDateFormat.getCalendar();
                            picker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                            Calendar maxCal = Calendar.getInstance();
                            maxCal.set(2030, 12, 31);
                            picker.setMaxDate(maxCal.getTimeInMillis());

                            builder.setView(picker)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            String birthday = picker.getDayOfMonth() + "/" + (picker.getMonth() + 1) + "/" + picker.getYear();

                                            SessionManager mSessionManager = new SessionManager(context);
                                            mSessionManager.setBirth(birthday);
                                            updateProfileInfo();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .create();

                            final AlertDialog alertDialog = builder.create();
                            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialogInterface) {
                                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                }
                            });
                            alertDialog.show();
                        }

                        break;

                    case R.id.cardViewGender:

                        if (context != null) {

                            CharSequence[] values = {"Male", "Female"};
                            int checked = -1;

                            switch (hmUser.get(KEY_GENDER) == null ? "null" : hmUser.get(KEY_GENDER).toString()) {
                                case "Male":
                                    checked = 0;
                                    break;
                                case "Female":
                                    checked = 1;
                                    break;
                                default:
                                    checked = -1;
                            }

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.CustomRadioGroupTheme));
                            } else {
                                builder = new AlertDialog.Builder(getActivity(), R.style.CustomRadioGroupTheme);
                            }

                            builder.setTitle("Gender")
                                    .setSingleChoiceItems(values, checked, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            switch (i) {
                                                case 0:
                                                    sessionManager.setGender("Male");
                                                    break;

                                                case 1:
                                                    sessionManager.setGender("Female");
                                                    break;

                                                default:
                                                    break;
                                            }
                                        }
                                    }).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            updateProfileInfo();
                                        }
                                    }).show();
                        }

                        break;

                    case R.id.cardViewHeight:

                        if (context != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.CustomAlertDialogWithEditText));
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                            }

                            input = new EditText(context);
                            input.setText(hmUser.get(KEY_HEIGHT) == null ? null : hmUser.get(KEY_HEIGHT).toString());
                            input.setInputType(InputType.TYPE_CLASS_NUMBER);
                            input.setMaxLines(1);
                            ColorStateList ctl = new ColorStateList(
                                    new int[][]{
                                            new int[]{android.R.attr.state_focused},
                                            new int[]{}
                                    },
                                    new int[]{
                                            context.getResources().getColor(R.color.colorPrimaryDark),
                                            context.getResources().getColor(R.color.colorAccent)
                                    }
                            );
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                input.setBackgroundTintList(ctl);
                            }

                            tvTitle = new TextView(context);
                            tvTitle.setText("Height (cm)");
                            tvTitle.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tvTitle.setPadding(20, 50, 10, 5);

                            builder.setCustomTitle(tvTitle)
                                    .setView(input)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            SessionManager mSessionManager = new SessionManager(context);
                                            mSessionManager.setHeight(input.getText().toString());
                                            updateProfileInfo();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    });

                            AlertDialog alertDialog = builder.create();
                            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                            alertDialog.show();
                        }

                        break;

                    case R.id.cardViewWeight:

                        if (context != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.CustomAlertDialogWithEditText));
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                            }

                            input = new EditText(context);
                            input.setText(hmUser.get(KEY_WEIGHT) == null ? null : hmUser.get(KEY_WEIGHT).toString());
                            input.setInputType(InputType.TYPE_CLASS_NUMBER);
                            input.setMaxLines(1);
                            ColorStateList ctl = new ColorStateList(
                                    new int[][]{
                                            new int[]{android.R.attr.state_focused},
                                            new int[]{}
                                    },
                                    new int[]{
                                            context.getResources().getColor(R.color.colorPrimaryDark),
                                            context.getResources().getColor(R.color.colorAccent)
                                    }
                            );
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                input.setBackgroundTintList(ctl);
                            }

                            tvTitle = new TextView(context);
                            tvTitle.setText("Weight (kg)");
                            tvTitle.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            tvTitle.setPadding(20, 50, 10, 5);

                            builder.setCustomTitle(tvTitle)
                                    .setView(input)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            SessionManager mSessionManager = new SessionManager(context);
                                            mSessionManager.setWeight(input.getText().toString());
                                            updateProfileInfo();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    });

                            AlertDialog alertDialog = builder.create();
                            alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                            alertDialog.show();
                        }

                        break;

                    case R.id.cardViewRace:

                        if (context != null) {

                            CharSequence[] values = {"Malay", "Chinese", "Indian", "Others"};
                            int checked = -1;

                            switch (hmUser.get(KEY_RACE) == null ? "null" : hmUser.get(KEY_RACE).toString()) {
                                case "Malay":
                                    checked = 0;
                                    break;
                                case "Chinese":
                                    checked = 1;
                                    break;
                                case "Indian":
                                    checked = 2;
                                    break;
                                case "Others":
                                    checked = 3;
                                    break;

                                default:
                                    checked = -1;
                            }

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.CustomRadioGroupTheme));
                            } else {
                                builder = new AlertDialog.Builder(getActivity(), R.style.CustomRadioGroupTheme);
                            }

                            builder.setTitle("Race")
                                    .setSingleChoiceItems(values, checked, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            switch (i) {
                                                case 0:
                                                    sessionManager.setRace("Malay");
                                                    break;

                                                case 1:
                                                    sessionManager.setRace("Chinese");
                                                    break;

                                                case 2:
                                                    sessionManager.setRace("Indian");
                                                    break;

                                                case 3:
                                                    sessionManager.setRace("Others");
                                                    break;

                                                default:
                                                    break;
                                            }
                                        }
                                    }).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    updateProfileInfo();
                                }
                            }).show();
                        }

                        break;

                    case R.id.btn_uploadImg:

                        intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Choose photo"), PICK_IMAGE_REQUEST);

                        break;

                    case R.id.btn_save:

                        ImageView imgProfile = getView().findViewById(R.id.profilePic);
                        TextView txtUsername = getView().findViewById(R.id.txtUsername);
                        TextView txtPhoneNo = getView().findViewById(R.id.txtUserPhoneNo);
                        TextView txtBirthDate = getView().findViewById(R.id.txtUserBirthDate);
                        TextView txtGender = getView().findViewById(R.id.txtUserGender);
                        TextView txtHeight = getView().findViewById(R.id.txtUserHeight);
                        TextView txtWeight = getView().findViewById(R.id.txtUserWeight);
                        TextView txtRace = getView().findViewById(R.id.txtUserRace);

                        // Validate if every fields are filled.
                        Drawable drawable = imgProfile.getDrawable();
                        Boolean hasImage = (drawable != null);

                        if (hasImage && (drawable instanceof BitmapDrawable)) {
                            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
                        }

                        Boolean validation = hasImage &&
                                !txtUsername.getText().toString().matches("") &&
                                !txtPhoneNo.getText().toString().matches("") &&
                                !txtBirthDate.getText().toString().matches("") &&
                                !txtGender.getText().toString().matches("") &&
                                !txtHeight.getText().toString().matches("") &&
                                !txtWeight.getText().toString().matches("") &&
                                !txtRace.getText().toString().matches("");

                        if (!validation) {
                                Toast.makeText(getActivity(), "Please make sure all fields are filled in.", Toast.LENGTH_LONG).show();
                        }
                        else {

                            // Data will be updated to Firebase when AccountFragment onDetach (refer: AccountFragment.java)

                            // Next step: Setup Pin Number
                            AlertFragment mAlertFragment = new AlertFragment();
                            FragmentTransaction mAlertTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            mAlertTransaction.replace(R.id.frame_setup, mAlertFragment);
                            mAlertTransaction.commit();

                            intent = new Intent(context, PinEntryActivity.class);
                            intent.putExtra("isSetup", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }

                        break;

                    case R.id.btn_signout:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.CustomRadioGroupTheme));
                        } else {
                            builder = new AlertDialog.Builder(getActivity(), R.style.CustomRadioGroupTheme);
                        }

                        builder.setMessage("Are you sure you want to logout?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, final int id) {
                                        AuthUI.getInstance()
                                                .signOut(context)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        sessionManager.clearSession();
                                                        Intent intent = new Intent(context, SignInActivity.class);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);

                                                    }
                                                });
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, final int id) {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();

                        break;

                    default:
                        break;
                }
            }
        };

        if(getActivity().getSupportFragmentManager().findFragmentByTag("SETUP_FRAGMENT") != null) {
            Button btnSave = getView().findViewById(R.id.btn_save);
            btnSave.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            btnSave.requestLayout();
            btnSave.setEnabled(true);
            btnSave.setOnClickListener(listener);

            Button btnSignout = getView().findViewById(R.id.btn_signout);
            btnSignout.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            btnSignout.requestLayout();
            btnSignout.setEnabled(true);
            btnSignout.setOnClickListener(listener);
        }

        getView().findViewById(R.id.cardViewName).setOnClickListener(listener);
        getView().findViewById(R.id.cardViewPhoneNo).setOnClickListener(listener);
        getView().findViewById(R.id.cardViewBirthday).setOnClickListener(listener);
        getView().findViewById(R.id.cardViewGender).setOnClickListener(listener);
        getView().findViewById(R.id.cardViewHeight).setOnClickListener(listener);
        getView().findViewById(R.id.cardViewWeight).setOnClickListener(listener);
        getView().findViewById(R.id.cardViewRace).setOnClickListener(listener);
        getView().findViewById(R.id.btn_uploadImg).setOnClickListener(listener);

        updateProfileInfo();

    }

    @Override
    public void onResume() {
        super.onResume();
        updateProfileInfo();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateProfileInfo();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            try {

                final Uri sourceUri = data.getData();

                try {

                    final File file = File.createTempFile("tempProfilePic", null, context.getCacheDir());
                    final Uri destinationUri = Uri.fromFile(file);

                    UCrop.Options options = new UCrop.Options();
                    options.setToolbarColor(getResources().getColor(R.color.colorPrimaryDark));
                    options.setStatusBarColor(getResources().getColor(android.R.color.black));
                    options.setCompressionQuality(100);
                    options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
                    options.setActiveWidgetColor(getResources().getColor(R.color.colorPrimaryDark));

                    UCrop.of(sourceUri, destinationUri)
                            .withAspectRatio(1, 1)
                            .withMaxResultSize(800, 800)
                            .withOptions(options)
                            .start(getActivity(), AccountFragment.this);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {

            final Uri resultUri = UCrop.getOutput(data);

            TextView tv_btnUpload = Objects.requireNonNull(getView()).findViewById(R.id.tv_btn_uploadImg);
            if(tv_btnUpload != null)
                tv_btnUpload.setText("Uploading...");

            // Upload to Firebase Storage
            if(resultUri != null) {
                UploadImageService.startActionProfileUpload(context, resultUri.toString());
                LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
                        new IntentFilter(UploadImageService.PROFILE_UPLOAD));
            }


        }

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("downloadUrl");
            Uri downloadUrl = Uri.parse(message);
            ImageView imageView = getView().findViewById(R.id.profilePic);

            Glide.with(AccountFragment.this)
                    .load(downloadUrl)
                    .into(imageView);

            TextView tv_btnUpload = getView().findViewById(R.id.tv_btn_uploadImg);

            int paddingDp = 400;
            float density = getContext().getResources().getDisplayMetrics().density;
            int paddingPixel = (int) (paddingDp * density);

            View btnUpload = getView().findViewById(R.id.btn_uploadImg);
            btnUpload.getLayoutParams().height = paddingPixel;

            tv_btnUpload.setText("");
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
        saveToFirebase();
    }

    public void updateProfileInfo() {

        hmUser = sessionManager.getUser();

        TextView txtUsername = getView().findViewById(R.id.txtUsername);
        TextView txtPhoneNo = getView().findViewById(R.id.txtUserPhoneNo);
        TextView txtBirthDate = getView().findViewById(R.id.txtUserBirthDate);
        TextView txtGender = getView().findViewById(R.id.txtUserGender);
        TextView txtHeight = getView().findViewById(R.id.txtUserHeight);
        TextView txtWeight = getView().findViewById(R.id.txtUserWeight);
        TextView txtRace = getView().findViewById(R.id.txtUserRace);


        if (hmUser != null) {
            txtUsername.setText(hmUser.get(KEY_NAME) == null ? "" : hmUser.get(KEY_NAME).toString());
            txtPhoneNo.setText(hmUser.get(KEY_PHONE) == null ? "" : hmUser.get(KEY_PHONE).toString());
            txtBirthDate.setText(hmUser.get(KEY_BIRTH) == null ? "" : hmUser.get(KEY_BIRTH).toString());
            txtGender.setText(hmUser.get(KEY_GENDER) == null ? "" : hmUser.get(KEY_GENDER).toString());
            txtHeight.setText(hmUser.get(KEY_HEIGHT) == null ? "" : hmUser.get(KEY_HEIGHT).toString());
            txtWeight.setText(hmUser.get(KEY_WEIGHT) == null ? "" : hmUser.get(KEY_WEIGHT).toString());
            txtRace.setText(hmUser.get(KEY_RACE) == null ? "" : hmUser.get(KEY_RACE).toString());
        }

        if (hmUser.get(KEY_DOWNLOAD_IMAGEURI) != null && !hmUser.get(KEY_DOWNLOAD_IMAGEURI).toString().equals("")) {

            Uri downloadUrl = Uri.parse(hmUser.get(KEY_DOWNLOAD_IMAGEURI).toString());
            ImageView img = getView().findViewById(R.id.profilePic);
            TextView tv_btnUpload = getView().findViewById(R.id.tv_btn_uploadImg);

            tv_btnUpload.setText("Downloading your profile picture...");
            Glide.with(AccountFragment.this)
                    .load(downloadUrl)
                    .into(img);

            int paddingDp = 400;
            float density = getContext().getResources().getDisplayMetrics().density;
            int paddingPixel = (int) (paddingDp * density);

            View btnUpload = getView().findViewById(R.id.btn_uploadImg);
            btnUpload.getLayoutParams().height = paddingPixel;

            tv_btnUpload.setText("");

        }

    }

    private void saveToFirebase() {
        if (databaseRef != null && hmUser != null)
            databaseRef.updateChildren(hmUser);
    }

    private boolean checkPermissionForWriteExtertalStorage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    private void requestPermissionForWriteExtertalStorage() throws Exception {
        try {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_STORAGE_PERMISSION_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
