package wenheloh.crowddistress;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.service.autofill.Dataset;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Objects;

import wenheloh.crowddistress.model.Contacts;
import wenheloh.crowddistress.helpers.SessionManager;
import wenheloh.crowddistress.model.DistressEvent;

import static wenheloh.crowddistress.helpers.SessionManager.KEY_SETUP;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private Intent intent;
    public static FirebaseUser currentUser;
    public static String PinNumber;
    public static SessionManager sessionManager;
    public LocationManager locationManager;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 100;
    private final int REQUEST_CHECK_SETTINGS = 200;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";

    private Boolean isLocationPermissionGranted = false;
    private GoogleApiClient googleApiClient;

    // For Draw Overlay Settings
    Boolean canDraw;
    private AppOpsManager.OnOpChangedListener onOpChangedListener = null;

    // For never ending service
    private Intent requestListenerIntent;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_account:
                    //Clear all fragment loaded in the container first
                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {

                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }

                    //Load new fragment
                    AccountFragment mAccountFragment = new AccountFragment();
                    FragmentTransaction mAccountTransaction = getSupportFragmentManager().beginTransaction();
                    mAccountTransaction.replace(R.id.fragment_container, mAccountFragment);
                    mAccountTransaction.commit();
                    return true;

                case R.id.navigation_alert:
                    //Clear all fragment loaded in the container first
                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }

                    FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();

                    // Load Alert fragment when PinNumber is fetched
                    if (PinNumber != null && !PinNumber.equals("")) {
                        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                        }

                        AlertFragment mAlertFragment = new AlertFragment();
                        mTransaction.replace(R.id.fragment_container, mAlertFragment);
                        mTransaction.commit();
                    }

                    return true;

                case R.id.navigation_events:
                    //Clear all fragment loaded in the container first
                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {

                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }

                    //Load new fragment
                    EventFragment mEventFragment = new EventFragment();
                    FragmentTransaction mEventTransaction = getSupportFragmentManager().beginTransaction();
                    mEventTransaction.replace(R.id.fragment_container, mEventFragment);
                    mEventTransaction.commit();

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Can Draw Overlay Setting
        if (savedInstanceState != null && Build.VERSION.SDK_INT >= 23) {
            canDraw = Settings.canDrawOverlays(this);
        }

        // Initialize Firebase & SessionManager
        FirebaseApp.initializeApp(this);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        googleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();

        // Redirect back to SignInActivity if login failed
        if (getIntent().getBooleanExtra("NotSignin", false)) {
            Intent intent = new Intent(MainActivity.this, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SessionManager sessionManager = new SessionManager(this);
            sessionManager.clearSession();
            startActivity(intent);
            finish();
        }

        if (currentUser == null) {
            // User not logged in
            intent = new Intent(MainActivity.this, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else if (!isNetworkAvailable()) {
            AlertDialog.Builder builder;

            // Setup AlertDialog
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.CustomAlertDialogWithEditText));
            } else {
                builder = new AlertDialog.Builder(MainActivity.this);
            }

            TextView tvTitle = new TextView(MainActivity.this);
            tvTitle.setText("No internet access, application is closing.");
            tvTitle.setPadding(20, 50, 10, 5);

            builder.setCustomTitle(tvTitle)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        } else {

            // Check if got Contact Request
            checkRequest();

            // Get Device token for FCM Push Notification
            String token = FirebaseInstanceId.getInstance().getToken();

            if (token != null) {
                FirebaseDatabase.getInstance().getReference()
                        .child("fcmTokens")
                        .child(currentUser.getUid())
                        .setValue(token);
            }

            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            // Show Loading screen first
            LaunchScreenFragment mLaunchScreenFragment = new LaunchScreenFragment();
            FragmentTransaction mLaunchScreenTransaction = getSupportFragmentManager().beginTransaction();
            mLaunchScreenTransaction.replace(R.id.fragment_container, mLaunchScreenFragment, "LAUNCH_SCREEN_FRAGMENT");
            mLaunchScreenTransaction.commit();
            getSupportFragmentManager().executePendingTransactions();

            sessionManager = new SessionManager(this);
            PinNumber = sessionManager.getPin();

            // Redirect to setup page if user haven't finish setting up their profile
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                    .child("users")
                    .child(currentUser.getUid())
                    .child(KEY_SETUP);

            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final String result = dataSnapshot.getValue() == null ? "" : dataSnapshot.getValue().toString();

                    if (result != null && result.equals("1")) {
                        Intent intent = new Intent(MainActivity.this, SetupAccountActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        finish();
                    }

                    // Redirect to DistressRingingFragment if contact is in emergency
                    String email = currentUser.getEmail().replaceAll("[@.]", "_");
                    DatabaseReference ringingRef = FirebaseDatabase.getInstance().getReference()
                            .child("contacts")
                            .child(email)
                            .child("ringing");

                    ringingRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {
                                Intent intent = new Intent(MainActivity.this, DistressRingingActivity.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

            /*
                ver 1: Get User's Pin Number every time startup, do not store locally
                ver 2 (08/04/2018): Pin store locally, get again if not available locally
            */
                    DatabaseReference pinRef = FirebaseDatabase.getInstance().getReference()
                            .child("users")
                            .child(currentUser.getUid())
                            .child("pin");

                    ValueEventListener pinListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            PinNumber = dataSnapshot.getValue() == null ? "" : dataSnapshot.getValue().toString();


                            // Request location permission if not granted
                            if (ContextCompat.checkSelfPermission(MainActivity.this,
                                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(Objects.requireNonNull(MainActivity.this),
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                            } else {
                                showGPSSettingDialog();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    };

                    pinRef.addListenerForSingleValueEvent(pinListener);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            initializeApp();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void showGPSSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        initializeApp();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.CustomAlertDialog));
                        builder.setMessage("Your GPS seems to be disabled, application is closing if GPS service is not provided.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, final int id) {
                                        finish();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();
                        break;
                }
            }
        });
    }

    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    isLocationPermissionGranted = true;
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    isLocationPermissionGranted = false;
                }

            }
        }
    };

    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showGPSSettingDialog();
        }
    };


    private void initializeApp() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        // Set the map's camera position to the current location of the device.
                        EventFragment.mLastKnownLocation = task.getResult();

                        if (getSupportFragmentManager().findFragmentByTag("LAUNCH_SCREEN_FRAGMENT") != null) {
                            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                            }


                            AlertFragment mAlertFragment = new AlertFragment();
                            FragmentTransaction mAlertTransaction = getSupportFragmentManager().beginTransaction();
                            mAlertTransaction.replace(R.id.fragment_container, mAlertFragment);
                            mAlertTransaction.commit();

                            initializeNavigationMenus();
                        }

                    } else if (task.getResult() == null) {
                        // Implicitly get location info once, in case FusedLocationProvider return null

                        try {
                            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER
                                    , new android.location.LocationListener() {
                                        @Override
                                        public void onLocationChanged(Location location) {
                                            EventFragment.mLastKnownLocation = location;

                                            if (getSupportFragmentManager().findFragmentByTag("LAUNCH_SCREEN_FRAGMENT") != null) {
                                                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                                                    getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                                                }


                                                AlertFragment mAlertFragment = new AlertFragment();
                                                FragmentTransaction mAlertTransaction = getSupportFragmentManager().beginTransaction();
                                                mAlertTransaction.replace(R.id.fragment_container, mAlertFragment);
                                                mAlertTransaction.commit();

                                                initializeNavigationMenus();
                                            }
                                        }

                                        @Override
                                        public void onStatusChanged(String s, int i, Bundle bundle) {

                                        }

                                        @Override
                                        public void onProviderEnabled(String s) {

                                        }

                                        @Override
                                        public void onProviderDisabled(String s) {

                                        }
                                    },
                                    null);
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void initializeNavigationMenus() {
        // Initialize top right Drawerlayout
        mDrawerLayout = findViewById(R.id.drawer_layout);

        // Initialize BottomNavigationView on resume
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Setup Drawer's items
        final NavigationView mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //item.setChecked(true);

                switch (item.getItemId()) {
                    case R.id.nav_home:
                        mDrawerLayout.closeDrawers();
                        break;

                    case R.id.nav_emergency_contacts:
                        intent = new Intent(MainActivity.this, AddContactActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.nav_setting:
                        intent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        break;

                    default:
                        break;
                }

                mDrawerLayout.closeDrawer(mNavigationView);

                return true;
            }
        });

        //Set Bottom Navigation Icons color
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_alert);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
    }

    private void checkRequest() {
        final String email = currentUser.getEmail().replaceAll("[@.]", "_");
        final String[] key = new String[1];

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child("contacts")
                .child(email)
                .child("request");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        final Contacts contacts = snapshot.getValue(Contacts.class);

                        key[0] = snapshot.getKey();

                        String message = contacts.getUsername() + " wanted to add you as an emergency contact. Accept?";

                        // Build and show alertdialog
                        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.CustomAlertDialog));
                        builder.setMessage(message)
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, final int id) {

                                        // Add to own Added node
                                        DatabaseReference ownContactsAddedRef = FirebaseDatabase.getInstance().getReference()
                                            .child("contacts")
                                            .child(email)
                                            .child("added")
                                            .child(key[0]);
                                        ownContactsAddedRef.keepSynced(true);
                                        ownContactsAddedRef.setValue(contacts);

                                        // Add to sender's Added node
                                        DatabaseReference senderContactsAddedRef = FirebaseDatabase.getInstance().getReference()
                                                .child("contacts")
                                                .child(contacts.getEmail())
                                                .child("added")
                                                .child(key[0]);

                                        HashMap<String, Object> hmAdded = new HashMap<>();
                                        hmAdded.put("email", email);
                                        hmAdded.put("uid", currentUser.getUid());
                                        hmAdded.put("imageUrl", sessionManager.getImageUrl());
                                        hmAdded.put("username", sessionManager.getName());

                                        senderContactsAddedRef.keepSynced(true);
                                        senderContactsAddedRef.setValue(hmAdded);

                                        // Delete the request after added
                                        DatabaseReference ownContactsRequestRef = FirebaseDatabase.getInstance().getReference()
                                                .child("contacts")
                                                .child(email)
                                                .child("request")
                                                .child(key[0]);
                                        ownContactsRequestRef.removeValue();

                                        DatabaseReference senderContactsPendingRef = FirebaseDatabase.getInstance().getReference()
                                                .child("contacts")
                                                .child(contacts.getEmail())
                                                .child("pending")
                                                .child(key[0]);
                                        senderContactsPendingRef.removeValue();

                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, final int id) {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!isNetworkAvailable()) {
            AlertDialog.Builder builder;

            // Setup AlertDialog
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.CustomAlertDialogWithEditText));
            } else {
                builder = new AlertDialog.Builder(MainActivity.this);
            }

            TextView tvTitle = new TextView(MainActivity.this);
            tvTitle.setText("No internet access, application is closing.");
            tvTitle.setPadding(20, 50, 10, 5);

            builder.setCustomTitle(tvTitle)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }

        // Check if logged in
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser == null) {

            // User not logged in
            intent = new Intent(MainActivity.this, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SessionManager sessionManager = new SessionManager(this);
            sessionManager.clearSession();
            startActivity(intent);
            finish();

        }

        // Redirect to DistressRingingFragment if contact is in emergency
        String email = currentUser.getEmail().replaceAll("[@.]", "_");
        DatabaseReference ringingRef = FirebaseDatabase.getInstance().getReference()
                .child("contacts")
                .child(email)
                .child("ringing");

        ringingRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Intent intent = new Intent(MainActivity.this, DistressRingingActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Check if Distress Service is running.
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child("users")
                .child(currentUser.getUid())
                .child("isAlarmTriggered");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(!dataSnapshot.getValue().toString().equals("false")) {
                    PinEntryActivity.isDistressServiceCalled = true;
                    Intent intent = new Intent(MainActivity.this, PinEntryActivity.class);
                    startActivity(intent);
                } else {
                    PinEntryActivity.isDistressServiceCalled = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // If is from SignInActivity
        if (getIntent().getBooleanExtra("fromSigninActivity", true))
            checkRequest();

        // Check if Draw Over Other Apps permission is granted every time onStart
        checkDrawOverPermission();

        // Register broadcast receiver
        registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));

        // Reconnect googleClientAPI
        if (googleApiClient != null) {
            googleApiClient.reconnect();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        //Unregister receiver on destroy
        try {
                unregisterReceiver(gpsLocationReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentByTag("LAUNCH_SCREEN_FRAGMENT") == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();

            BottomNavigationView mBottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
            Fragment mFragment = fragmentManager.findFragmentById(R.id.fragment_container);

            if (mFragment instanceof AccountFragment)
                mBottomNavigationView.setSelectedItemId(R.id.navigation_alert);
            else if (mFragment instanceof AlertFragment)
                super.onBackPressed();
            else if (mFragment instanceof EventFragment)
                mBottomNavigationView.setSelectedItemId(R.id.navigation_alert);
        } else {
            super.onBackPressed();
        }
    }

    private void checkDrawOverPermission(){
        final int version = Build.VERSION.SDK_INT;

        if (version >= 23 && canDraw != null && !canDraw) {
            final AppOpsManager opsManager = (AppOpsManager) getApplicationContext().getSystemService(Context.APP_OPS_SERVICE);

            final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.CustomAlertDialog));
            builder.setMessage("Please allow this app to draw over other app, press Yes to redirect to Settings page")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {

                            onOpChangedListener = new AppOpsManager.OnOpChangedListener() {

                                @Override
                                public void onOpChanged(String op, String packageName) {

                                    PackageManager packageManager = getPackageManager();
                                    String myPackageName = getPackageName();
                                    if (myPackageName.equals(packageName) &&
                                            AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW.equals(op)) {
                                        canDraw = !canDraw;
                                    }
                                }

                            };

                            opsManager.startWatchingMode(AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW,
                                    null, onOpChangedListener);

                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getApplicationContext().getPackageName()));

                            /* request permission via start activity */
                            startActivityForResult(intent, 1);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {
                            finish();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();

        } else if(version >= 23 && canDraw == null) {
            final AppOpsManager opsManager = (AppOpsManager) getApplicationContext().getSystemService(Context.APP_OPS_SERVICE);
            canDraw = Settings.canDrawOverlays(getApplicationContext());

            if (!canDraw) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.CustomAlertDialog));
                builder.setMessage("Please allow this app to draw over other app, press Yes to redirect to Settings page")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {

                                onOpChangedListener = new AppOpsManager.OnOpChangedListener() {

                                    @Override
                                    public void onOpChanged(String op, String packageName) {

                                        PackageManager packageManager = getPackageManager();
                                        String myPackageName = getPackageName();
                                        if (myPackageName.equals(packageName) &&
                                                AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW.equals(op)) {
                                            canDraw = !canDraw;
                                        }
                                    }

                                };

                                opsManager.startWatchingMode(AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW,
                                        null, onOpChangedListener);

                                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                        Uri.parse("package:" + getApplicationContext().getPackageName()));

                                /* request permission via start activity */
                                startActivityForResult(intent, 1);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                finish();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (onOpChangedListener != null) {
                AppOpsManager opsManager = (AppOpsManager)getApplicationContext().getSystemService(Context.APP_OPS_SERVICE);
                opsManager.stopWatchingMode(onOpChangedListener);
                onOpChangedListener = null;
            }
        } else if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case RESULT_OK:
                    initializeApp();
                    break;
                case RESULT_CANCELED:
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.CustomAlertDialog));
                    builder.setMessage("Your GPS seems to be disabled, application is closing if GPS service is not provided.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int id) {
                                    finish();
                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        isLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isLocationPermissionGranted = true;
                    initializeApp();
                } else if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.CustomAlertDialog));
                    builder.setMessage("GPS permission must be granted, application is closing.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int id) {
                                    finish();
                                }
                            }).show();
                }
            }
        }
    }
}
