package wenheloh.crowddistress;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import wenheloh.crowddistress.helpers.SessionManager;
import wenheloh.crowddistress.services.UploadImageService;

import static wenheloh.crowddistress.helpers.SessionManager.TAKE_PHOTO_REQUEST;

public class ReportEventActivity extends AppCompatActivity {

    String mCurrentPhotoPath;
    ImageView mImageView;
    SessionManager sessionmanager;
    FirebaseUser currentUser;
    Boolean isReporting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_event);

        FirebaseApp.initializeApp(this);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        sessionmanager = new SessionManager(this);

        mImageView = findViewById(R.id.imgbtn_takePhoto);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.imgbtn_takePhoto:
                        dispatchTakePhotoIntent();
                        break;

                    case R.id.btnCancel:
                        finish();
                        onBackPressed();
                        break;

                    case R.id.btnSubmit:

                        // Validate if every fields are filled.
                        ImageView imageView = findViewById(R.id.iv_result);
                        Drawable drawable = imageView.getDrawable();
                        Boolean hasImage = (drawable != null);

                        if (hasImage && (drawable instanceof BitmapDrawable)) {
                            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
                        }

                        Spinner spnType = findViewById(R.id.spn_eventType);
                        String selectedType = spnType.getSelectedItem().toString();

                        Boolean validation = hasImage && !selectedType.equals("Choose the type of event");

                        if (validation) {
                            //  Update UI
                            ReportEventActivity.this.setFinishOnTouchOutside(false);

                            // Convert dp to pixel
                            int paddingDp = 200;
                            float density = ReportEventActivity.this.getResources().getDisplayMetrics().density;
                            int paddingPixel = (int) (paddingDp * density);

                            LinearLayout layout_entries = findViewById(R.id.layout_entries);
                            LinearLayout layout_buttons = findViewById(R.id.layout_buttons);
                            LinearLayout layout_splash = findViewById(R.id.layout_splash);

                            layout_entries.getLayoutParams().height = 0;
                            layout_buttons.getLayoutParams().height = 0;
                            layout_splash.getLayoutParams().height = paddingPixel;

                            layout_entries.requestLayout();
                            layout_buttons.requestLayout();
                            layout_splash.requestLayout();

                            // Start uploading
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("events").child("normal");
                            String eventid = ref.push().getKey();

                            UploadImageService.startActionEventUpload(ReportEventActivity.this, eventid, selectedType, mCurrentPhotoPath);
                            LocalBroadcastManager.getInstance(ReportEventActivity.this).registerReceiver(mMessageReceiver,
                                    new IntentFilter(UploadImageService.EVENT_UPLOAD));

                        } else {
                            Toast.makeText(ReportEventActivity.this, "Please ensure all fields are filled up.", Toast.LENGTH_SHORT).show();
                        }

                        break;
                }
            }
        };

        findViewById(R.id.imgbtn_takePhoto).setOnClickListener(listener);
        findViewById(R.id.btnCancel).setOnClickListener(listener);
        findViewById(R.id.btnSubmit).setOnClickListener(listener);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //String eventId = intent.getStringExtra("eventId");

            LinearLayout layout_entries = findViewById(R.id.layout_entries);
            LinearLayout layout_buttons = findViewById(R.id.layout_buttons);
            LinearLayout layout_splash = findViewById(R.id.layout_splash);

            layout_entries.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
            layout_buttons.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
            layout_splash.getLayoutParams().height = 0;

            layout_entries.requestLayout();
            layout_buttons.requestLayout();
            layout_splash.requestLayout();

            onBackPressed();

        }
    };

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePhotoIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                // Error occurred while creating the File
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TAKE_PHOTO_REQUEST && resultCode == RESULT_OK) {

            ImageButton imgbtn = findViewById(R.id.imgbtn_takePhoto);

            //Toast.makeText(this, String.valueOf(f.length()), Toast.LENGTH_SHORT).show();

            // Convert dp to pixel
            int paddingDp = 150;
            float density = this.getResources().getDisplayMetrics().density;
            int paddingPixel = (int) (paddingDp * density);

            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

            ImageView imgView = findViewById(R.id.iv_result);
            imgView.getLayoutParams().height = paddingPixel;
            imgView.setImageBitmap(bitmap);
            imgView.requestLayout();

            imgbtn.getLayoutParams().height = 0;
            imgbtn.requestLayout();

        }
    }

    @Override
    public void onBackPressed() {
        if(isReporting){
            return;
        }
        super.onBackPressed();
    }
}
