package wenheloh.crowddistress.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import wenheloh.crowddistress.MainActivity;
import wenheloh.crowddistress.R;
import wenheloh.crowddistress.model.Contacts;

public class AddedContactsArrayAdapter extends ArrayAdapter<Contacts> {

    private Context context;
    private ArrayList<Contacts> contacts;

    public AddedContactsArrayAdapter(Context context, ArrayList<Contacts> contacts) {
        super(context, 0, contacts);
        this.context = context;
        this.contacts = contacts;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Contacts contact = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.emergency_contact_list_layout, parent, false);
        }

        // findView
        ImageView iv_profile = convertView.findViewById(R.id.roundedImageView);
        TextView tv_username = convertView.findViewById(R.id.tv_username);
        TextView tv_email = convertView.findViewById(R.id.tv_email);
        Button btn_delete = convertView.findViewById(R.id.btn_delete);

        // load image
        Glide.with(this.getContext())
                .load(contact.getImageUrl())
                .into(iv_profile);

        tv_username.setText(contact.getUsername());

        String[] emailParts = contact.getEmail().split("_");
        String email = "(" + emailParts[0] + "@" + emailParts[1] + "." + emailParts[2] + ")";
        tv_email.setText(email);

        btn_delete.setVisibility(View.VISIBLE);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.CustomAlertDialog));
                builder.setMessage("Are you sure you want to delete this pending request?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, final int id) {
                                DatabaseReference ownContactsRequestRef = FirebaseDatabase.getInstance().getReference()
                                        .child("contacts")
                                        .child(contact.getEmail())
                                        .child("added")
                                        .child(contact.getRequestId());
                                ownContactsRequestRef.removeValue();

                                String currentEmail = MainActivity.currentUser.getEmail().replaceAll("[@.]", "_");

                                DatabaseReference senderContactsPendingRef = FirebaseDatabase.getInstance().getReference()
                                        .child("contacts")
                                        .child(currentEmail)
                                        .child("added")
                                        .child(contact.getRequestId());
                                senderContactsPendingRef.removeValue();

                                remove(contact);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();
            }
        });

        return convertView;
    }

}
