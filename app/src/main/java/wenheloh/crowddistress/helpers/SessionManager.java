package wenheloh.crowddistress.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by wenheloh on 21-Mar-18.
 */

public class SessionManager {
    private static SharedPreferences sharedPreferences;
    private static final String FILE = "Crowd_Distress_User_Session";
    private static SharedPreferences.Editor editor;

    // Also as a class to store Constant (LOL)
    public static final String KEY_NAME = "name";
    public static final String KEY_BIRTH = "date";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_HEIGHT = "height";
    public static final String KEY_WEIGHT = "weight";
    public static final String KEY_RACE = "race";
    public static final String KEY_DOWNLOAD_IMAGEURI = "imageUrl";
    public static final String KEY_PIN = "pin";
    public static final String KEY_PHONE = "phoneNo";
    public static final String KEY_SETUP = "isSetup";

    public static final int RC_SIGN_IN = 1;
    public static final int PICK_IMAGE_REQUEST = 2;
    public static final int TAKE_PHOTO_REQUEST = 3;

    // DistressService Constant
    public interface ACTION {
        String STARTFOREGROUND_ACTION = "wenheloh.crowddistress.action.startforeground";
        String STOPFOREGROUND_ACTION = "wenheloh.crowddistress.action.stopforeground";
        String ACTION_PROFILE_UPLOAD = "wenheloh.crowddistress.helpers.action.PROFILE_UPLOAD";
        String ACTION_EVENT_UPLOAD = "wenheloh.crowddistress.helpers.action.EVENT_UPLOAD";
        String ACTION_DISTRESS_UPLOAD = "wenheloh.crowddistress.helpers.action.DISTRESS_UPLOAD";
    }


    public interface CHANNEL_ID {
        String id = "wenheloh.crowddistress.distress-signal";
    }


    public SessionManager(Context context) {
        sharedPreferences = context.getSharedPreferences(FILE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void saveUser(String name, String date, String gender, String height, String weight, String race, String image, String pin, String phoneNo) {
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_BIRTH, date);
        editor.putString(KEY_GENDER, gender);
        editor.putString(KEY_HEIGHT, height);
        editor.putString(KEY_WEIGHT, weight);
        editor.putString(KEY_RACE, race);
        editor.putString(KEY_DOWNLOAD_IMAGEURI, image);
        editor.putString(KEY_PIN, pin);
        editor.putString(KEY_PHONE, phoneNo);

        editor.commit();
    }

    public HashMap<String, Object> getUser() {
        HashMap<String, Object> hmUser = new HashMap<>();

        hmUser.put(KEY_NAME, sharedPreferences.getString(KEY_NAME, null));
        hmUser.put(KEY_BIRTH, sharedPreferences.getString(KEY_BIRTH, null));
        hmUser.put(KEY_GENDER, sharedPreferences.getString(KEY_GENDER, null));
        hmUser.put(KEY_WEIGHT, sharedPreferences.getString(KEY_WEIGHT, null));
        hmUser.put(KEY_HEIGHT, sharedPreferences.getString(KEY_HEIGHT, null));
        hmUser.put(KEY_RACE, sharedPreferences.getString(KEY_RACE, null));
        hmUser.put(KEY_DOWNLOAD_IMAGEURI, sharedPreferences.getString(KEY_DOWNLOAD_IMAGEURI, null));
        hmUser.put(KEY_PHONE, sharedPreferences.getString(KEY_PHONE, null));

        return hmUser;

    }

    public void setName(String name) {
        editor.putString(KEY_NAME, name);
        editor.commit();
    }

    public String getName() {
        return sharedPreferences.getString(KEY_NAME, null);
    }

    public void setBirth(String date) {
        editor.putString(KEY_BIRTH, date);
        editor.commit();
    }

    public void setGender(String gender) {
        editor.putString(KEY_GENDER, gender);
        editor.commit();
    }

    public void setWeight(String weight) {
        editor.putString(KEY_WEIGHT, weight);
        editor.commit();
    }

    public void setHeight(String height) {
        editor.putString(KEY_HEIGHT, height);
        editor.commit();
    }

    public void setRace(String race) {
        editor.putString(KEY_RACE, race);
        editor.commit();
    }

    public void setDownloadImageUri(String uri) {
        editor.putString(KEY_DOWNLOAD_IMAGEURI, uri);
        editor.commit();
    }

    public void setPin(String pin) {
        editor.putString(KEY_PIN, pin);
        editor.commit();
    }

    public String getPin() {

        if (sharedPreferences.getString(KEY_PIN, null) == null)
            return "";
        else
            return sharedPreferences.getString(KEY_PIN, null);

    }

    public String getImageUrl() {
        return sharedPreferences.getString(KEY_DOWNLOAD_IMAGEURI, null);
    }

    public void setPhone(String phone) {
        editor.putString(KEY_PHONE, phone);
        editor.commit();
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }
}
