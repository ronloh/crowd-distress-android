package wenheloh.crowddistress;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;

import wenheloh.crowddistress.helpers.SessionManager;

public class SettingsActivity extends AppCompatActivity {
    private SessionManager sessionManager;
    private HashMap<Object, Object> hmUser;
    private String username;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupActionBar();

        sessionManager = new SessionManager(this);

        // Setup OnClickListener for the buttons
        View.OnClickListener listener = new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {

                TextView tvTitle;
                final EditText input;

                switch (view.getId()) {

                    case R.id.cv_btn_logout:

                        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(SettingsActivity.this, R.style.CustomAlertDialog));
                        builder.setMessage("Are you sure you want to logout?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, final int id) {
                                        AuthUI.getInstance()
                                                .signOut(SettingsActivity.this)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        sessionManager.clearSession();
                                                        Intent intent = new Intent(SettingsActivity.this, SignInActivity.class);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        finish();

                                                    }
                                                });
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog, final int id) {
                                        dialog.cancel();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();

                        break;

                    case R.id.cv_btn_change_pin:

                        if(MainActivity.PinNumber == null || MainActivity.PinNumber.equals("")) {
                            Toast.makeText(SettingsActivity.this, "Still fetching PIN Number...", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent changePinIntent = new Intent(SettingsActivity.this, PinEntryActivity.class);
                            changePinIntent.putExtra("isChangePin", "1");
                            startActivity(changePinIntent);
                        }

                        break;
                }
            }
        };

        findViewById(R.id.cv_btn_logout).setOnClickListener(listener);
        findViewById(R.id.cv_btn_change_pin).setOnClickListener(listener);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        // Respond to the action bar's Up/Home button
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setBackgroundDrawable(new ColorDrawable(Color.rgb(66, 165, 245)));
        }
    }

    /* Example get Userinfo, do not delete
        sessionManager = new SessionManager(getActivity().getApplicationContext());
        hmUser = sessionManager.getUser();
        username = (String) hmUser.get(KEY_NAME);

        if(username == null) {
            // go to login
        }
        */

    /* Example for SessionManager
    public void saveUser() {
        String name = "wenhe";
        String age = "24";
        String gender = "male";

        sessionManager.saveUser(name, age, gender);
    }

    public void logout() {

        sessionManager.clearSession();
    }
    */
}