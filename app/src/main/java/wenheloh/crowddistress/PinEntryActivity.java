package wenheloh.crowddistress;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.todddavies.components.progressbar.ProgressWheel;

import java.util.Date;
import java.util.HashMap;

import wenheloh.crowddistress.helpers.SessionManager;
import wenheloh.crowddistress.model.Contacts;
import wenheloh.crowddistress.services.DistressService;

import static wenheloh.crowddistress.helpers.SessionManager.KEY_SETUP;

public class PinEntryActivity extends AppCompatActivity {

    private static final String TAG = "DebugDistress";

    static int countdownTime = 3000;

    private ProgressWheel pw;
    private String displayTime;
    private PinEntryEditText pinEntry;
    private FirebaseUser currentUser;
    private Boolean isSetup;
    private String firstPin, secondPin;
    private SessionManager sessionManager;
    private String distressEventKey;

    // Boolean for alarm
    private Boolean isAlarmTriggered = true;
    public static Boolean isDistressServiceCalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_entry);

        this.setFinishOnTouchOutside(false);
        sessionManager = new SessionManager(this);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        // Calling from SetupActivity
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString("isSetup") != null) {

            isSetup = true;
            this.setFinishOnTouchOutside(true);

            // Update UI
            pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);
            ProgressWheel pw = findViewById(R.id.timer_countdown);
            pw.getLayoutParams().height = 0;
            pw.requestLayout();
            ImageView logo = findViewById(R.id.logo);
            logo.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            logo.setScaleX(2);
            logo.setScaleY(2);
            logo.requestLayout();

            // Setup New user Pin entry listener
            if (pinEntry != null) {
                pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                    @Override
                    public void onPinEntered(CharSequence str) {

                        if (firstPin == null || firstPin.equals("")) {
                            firstPin = str.toString();
                            pinEntry.setText("");
                            TextView tv_warning = findViewById(R.id.tv_warning);
                            tv_warning.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            tv_warning.setText("Confirm your PIN again");
                            tv_warning.requestLayout();
                        } else {

                            secondPin = str.toString();

                            if (secondPin.equals(firstPin)) {

                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
                                HashMap<String, Object> updatePin = new HashMap<>();
                                updatePin.put("pin", firstPin);
                                updatePin.put(KEY_SETUP, "0");

                                ref.updateChildren(updatePin);

                                firstPin = "";
                                secondPin = "";

                                // Get Device token for FCM Push Notification
                                String token = FirebaseInstanceId.getInstance().getToken();

                                if (token != null) {
                                    FirebaseDatabase.getInstance().getReference()
                                            .child("fcmTokens")
                                            .child(currentUser.getUid())
                                            .setValue(token);
                                }

                                Intent intent = new Intent(PinEntryActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                                finish();

                            } else {
                                TextView tv_warning = findViewById(R.id.tv_warning);
                                tv_warning.setTextColor(getResources().getColor(R.color.colorRed));
                                tv_warning.setText("PIN are not the same");
                                pinEntry.setText("");

                                // Vibrate when PIN incorrect
                                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                v.vibrate(300);
                            }
                        }
                    }
                });
            }
        } else if (bundle != null && bundle.getString("isChangePin") != null) {

            this.setFinishOnTouchOutside(true);

            // Update UI
            pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);
            ProgressWheel pw = findViewById(R.id.timer_countdown);
            pw.getLayoutParams().height = 0;
            pw.requestLayout();
            ImageView logo = findViewById(R.id.logo);
            logo.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            logo.setScaleX(2);
            logo.setScaleY(2);
            logo.requestLayout();

            TextView tv_warning = findViewById(R.id.tv_warning);
            tv_warning.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            tv_warning.requestLayout();
            tv_warning.setText("Enter current PIN");

            // Setup Change Pin entry listener
            if (pinEntry != null) {
                pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                    @Override
                    public void onPinEntered(CharSequence str) {

                        TextView tv_warning = findViewById(R.id.tv_warning);

                        Log.i(TAG, "First PIN: " + firstPin);
                        Log.i(TAG, "Second PIN: " + secondPin);

                        if (firstPin == null || firstPin.equals("")) {

                            firstPin = str.toString();
                            Log.i(TAG, "Updated First PIN: " + firstPin);
                            pinEntry.setText("");

                            if (!firstPin.equals(MainActivity.PinNumber)) {
                                // Vibrate when PIN incorrect
                                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                v.vibrate(300);
                                tv_warning.setText("PIN not match!");
                                tv_warning.setTextColor(getResources().getColor(R.color.colorRed));
                                firstPin = "";
                            } else {
                                tv_warning.setText("Enter new PIN");
                                tv_warning.setTextColor(getResources().getColor(R.color.color292929));
                                Log.i(TAG, "First PIN when enter new pin: " + firstPin);
                            }

                        } else if ( secondPin == null || secondPin.equals("") ) {

                            secondPin = str.toString();
                            Log.i(TAG, "Updated Second PIN: " + secondPin);
                            pinEntry.setText("");

                            tv_warning.setText("Enter one more time");
                            tv_warning.setTextColor(getResources().getColor(R.color.color292929));

                        } else {

                            String repeat = str.toString();
                            Log.i(TAG, "Repeat: " + repeat);
                            pinEntry.setText("");

                            if (!secondPin.equals(repeat)) {
                                // Vibrate when PIN incorrect
                                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                v.vibrate(300);
                                tv_warning.setText("PIN not match!");
                                tv_warning.setTextColor(getResources().getColor(R.color.colorRed));

                            } else {

                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
                                HashMap<String, Object> updatePin = new HashMap<>();
                                updatePin.put("pin", repeat);

                                ref.updateChildren(updatePin);

                                MainActivity.PinNumber = repeat;

                                Toast.makeText(PinEntryActivity.this, "PIN update successfully,", Toast.LENGTH_SHORT).show();
                                finish();

                            }

                        }
                    }
                });
            }
        } else {
            showCountDownTimer();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isSetup != null && !isSetup)
            showCountDownTimer();
    }

    private void showCountDownTimer() {
        pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);
        pw = (ProgressWheel) findViewById(R.id.timer_countdown);

        if (isDistressServiceCalled) {
            // Show different UI
            ImageView logo = findViewById(R.id.logo);
            logo.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            logo.setScaleX(2);
            logo.setScaleY(2);
            logo.requestLayout();

            ProgressWheel pw = findViewById(R.id.timer_countdown);
            pw.getLayoutParams().height = 0;
            pw.requestLayout();
        } else {

            new CountDownTimer(countdownTime, 1) {
                @Override
                public void onTick(long timeLeft) {
                    displayTime = String.format("%.2f", (float) timeLeft / 1000);
                    PinEntryActivity.this.pw.setProgress(360);

                    if (timeLeft < 1000)
                        PinEntryActivity.this.pw.setBarColor(Color.RED);
                    else if (timeLeft < 2000)
                        PinEntryActivity.this.pw.setBarColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));
                    else if (timeLeft < 3000)
                        PinEntryActivity.this.pw.setBarColor(Color.RED);

                    PinEntryActivity.this.pw.setText(displayTime);
                }

                @Override
                public void onFinish() {
                    PinEntryActivity.this.pw.setText("0");
                    PinEntryActivity.this.pw.setProgress(360);
                    PinEntryActivity.this.pw.setBarColor(Color.RED);

                    if (isAlarmTriggered) {
                        // Show different UI
                        ImageView logo = findViewById(R.id.logo);
                        logo.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        logo.setScaleX(2);
                        logo.setScaleY(2);
                        logo.requestLayout();

                        ProgressWheel pw = findViewById(R.id.timer_countdown);
                        pw.getLayoutParams().height = 0;
                        pw.requestLayout();

                        // Save info into database
                        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference()
                                .child("events")
                                .child("distress");
                        distressEventKey = eventRef.push().getKey();
                        eventRef = eventRef.child(distressEventKey);

                        // Update boolean in user node to event key
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                .child("users")
                                .child(currentUser.getUid())
                                .child("isAlarmTriggered");

                        ref.setValue(distressEventKey);

                        if (EventFragment.mLastKnownLocation == null && ContextCompat.checkSelfPermission(PinEntryActivity.this,
                                android.Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {
                            FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(PinEntryActivity.this);
                            Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                            locationResult.addOnCompleteListener(PinEntryActivity.this, new OnCompleteListener<Location>() {
                                @Override
                                public void onComplete(@NonNull Task<Location> task) {
                                    if (task.isSuccessful() && task.getResult() != null) {
                                        // Set the map's camera position to the current location of the device.
                                        EventFragment.mLastKnownLocation = task.getResult();
                                    } else {
                                        Log.e("Exception: %s", "Exception: %s", task.getException());
                                    }
                                }
                            });
                        }

                        if(EventFragment.mLastKnownLocation != null) {
                            final HashMap<String, Object> hmDistress = new HashMap<>();
                            hmDistress.put("lat", EventFragment.mLastKnownLocation.getLatitude());
                            hmDistress.put("lng", EventFragment.mLastKnownLocation.getLongitude());
                            hmDistress.put("timestamp", new Date().getTime());
                            hmDistress.put("type", "Distress Signal");
                            hmDistress.put("username", sessionManager.getName());
                            hmDistress.put("uid", currentUser.getUid());

                            eventRef.updateChildren(hmDistress);



                            // Start service for notification and further actions
                            Intent serviceIntent = new Intent(getApplicationContext(), DistressService.class);
                            serviceIntent.setAction(SessionManager.ACTION.STARTFOREGROUND_ACTION);
                            serviceIntent.putExtra("eventid", distressEventKey);
                            startService(serviceIntent);
                            isDistressServiceCalled = true;

                            // Trigger SMS for Cloud Function
                            DatabaseReference smsRef = FirebaseDatabase.getInstance().getReference()
                                    .child("sms");
                            smsRef.push().setValue(true);

                            // Record in Emergency Contact's node
                            String email = currentUser.getEmail().replaceAll("[@.]", "_");

                            DatabaseReference contactRef = FirebaseDatabase.getInstance().getReference()
                                    .child("contacts")
                                    .child(email)
                                    .child("added");

                            contactRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.getChildren() != null) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                                            Contacts contact = snapshot.getValue(Contacts.class);

                                            DatabaseReference ringingRef = FirebaseDatabase.getInstance().getReference()
                                                    .child("contacts")
                                                    .child(contact.getEmail())
                                                    .child("ringing")
                                                    .child(distressEventKey);

                                            ringingRef.setValue(hmDistress);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                        } else {
                            Log.e("Exception", "Failed to get location");
                        }
                    }
                }
            }.start();
        }

        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().equals(MainActivity.PinNumber)) {
                        isAlarmTriggered = false;

                        isDistressServiceCalled = false;

                        final DatabaseReference eventIdRef = FirebaseDatabase.getInstance().getReference()
                                .child("users")
                                .child(currentUser.getUid())
                                .child("isAlarmTriggered");

                        eventIdRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                distressEventKey = dataSnapshot.getValue().toString();

                                Intent serviceIntent = new Intent(getApplicationContext(), DistressService.class);
                                serviceIntent.setAction(SessionManager.ACTION.STOPFOREGROUND_ACTION);
                                serviceIntent.putExtra("eventid", distressEventKey);
                                startService(serviceIntent);

                                String email = currentUser.getEmail().replaceAll("[@.]", "_");
                                DatabaseReference contactRef = FirebaseDatabase.getInstance().getReference()
                                        .child("contacts")
                                        .child(email)
                                        .child("added");

                                contactRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.getChildren() != null) {
                                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                                                Contacts contact = snapshot.getValue(Contacts.class);

                                                DatabaseReference ringingRef = FirebaseDatabase.getInstance().getReference()
                                                        .child("contacts")
                                                        .child(contact.getEmail())
                                                        .child("ringing")
                                                        .child(distressEventKey);

                                                ringingRef.removeValue();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                                eventIdRef.setValue("false");
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                        onBackPressed();
                    } else {
                        Toast.makeText(PinEntryActivity.this, "PIN is incorrect", Toast.LENGTH_SHORT).show();
                        pinEntry.setText("");

                        // Vibrate when PIN incorrect
                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(300);
                    }
                }
            });
        }

        pw.stopSpinning();
    }

    @Override
    public void onBackPressed() {
        if (!isAlarmTriggered)
            super.onBackPressed();

        return;
    }

}
