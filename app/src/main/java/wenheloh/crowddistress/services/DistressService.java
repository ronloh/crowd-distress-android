package wenheloh.crowddistress.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import wenheloh.crowddistress.MainActivity;
import wenheloh.crowddistress.PinEntryActivity;
import wenheloh.crowddistress.R;
import wenheloh.crowddistress.helpers.SessionManager;

import static wenheloh.crowddistress.services.DistressCameraService.ACTION_START;

public class DistressService extends Service {

    public static MediaPlayer mp;
    private NotificationManager notificationManager;
    private final int notificationId = 666;
    private String databaseKey;

    public DistressService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (SessionManager.ACTION.STARTFOREGROUND_ACTION.equals(intent.getAction())) {

            PinEntryActivity.isDistressServiceCalled = true;

            databaseKey = intent.getStringExtra("eventid");

            // Use another Service to take photos, in case this Service is stopped before photos are taken.
            Intent serviceIntent = new Intent(getApplicationContext(), DistressCameraService.class);
            serviceIntent.setAction(ACTION_START);
            serviceIntent.putExtra("eventid", databaseKey);
            startService(serviceIntent);

            // Turn volume to Max
            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            try{
                am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            } catch(NullPointerException e) {
                e.printStackTrace();
            }

            // Start playing siren sound
            mp = MediaPlayer.create(DistressService.this, R.raw.siren);
            mp.setLooping(true);
            mp.start();

            // Call createNotification()
            createNotification();

        } else if (SessionManager.ACTION.STOPFOREGROUND_ACTION.equals(intent.getAction())) {

            databaseKey = intent.getStringExtra("eventid");

            notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);

            if (mp != null) {
                mp.stop();
                mp.release();
            }

            final DatabaseReference distressRef = FirebaseDatabase.getInstance().getReference()
                    .child("events")
                    .child("distress")
                    .child(databaseKey);

            distressRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    DatabaseReference pastDistressRef = FirebaseDatabase.getInstance().getReference()
                            .child("events")
                            .child("pastDistress")
                            .child(databaseKey);
                    pastDistressRef.keepSynced(true);

                    pastDistressRef.setValue(dataSnapshot.getValue());

                    distressRef.removeValue();

                    stopSelf();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        }

        return START_STICKY;
    }

    private void createNotification() {

        notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = "Crowd Distress";
            String description = "Distress signal";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(SessionManager.CHANNEL_ID.id, name, importance);
            channel.setDescription(description);
            // Register the channel with the system
            notificationManager.createNotificationChannel(channel);
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, SessionManager.CHANNEL_ID.id)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Distress signal is sent!")
                .setContentText("Press to disarm")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOngoing(true)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_ALARM);

        notificationManager.notify(notificationId, mBuilder.build());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
