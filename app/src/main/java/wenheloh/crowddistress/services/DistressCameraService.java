package wenheloh.crowddistress.services;

import android.Manifest;
import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Policy;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DistressCameraService extends Service {
    public static final String ACTION_START = "wenheloh.crowddistress.services.action.START";

    private String mCurrentPhotoPath;
    private SurfaceView preview;
    private WindowManager windowManager;
    private CaptureRequest.Builder captureRequestBuilder;
    private CameraDevice mCamera;
    private int pictureCounter = 0;
    private static int cameraDelay = 200;
    private Camera camera;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_START.equals(intent.getAction())) {
            final String databaseKey = intent.getStringExtra("eventid");
            handleActionStart(databaseKey);
        }

        return START_STICKY;
    }

    private void handleActionStart(String eventid) {
        preview = new SurfaceView(this);

        takePhoto(this, eventid);

    }

    private void takePhoto(final Context context, final String eventid) {

        SurfaceHolder holder = preview.getHolder();

        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            //The preview must happen at or after this point or takePicture fails
            public void surfaceCreated(final SurfaceHolder holder) {

                Camera camera = null;

                try {
                    camera = Camera.open();
                    try {
                        camera.setPreviewDisplay(holder);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }

                    camera.startPreview();

                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);

                    camera.setParameters(parameters);

                    camera.takePicture(null, null, new Camera.PictureCallback() {

                        @Override
                        public void onPictureTaken(byte[] data, Camera camera) {

                            // Create new photo file directory
                            File pictureDir = null;

                            try {
                                pictureDir = createImageFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            // Save to the directory if file is created
                            if (pictureDir != null) {
                                try{
                                    FileOutputStream fos = new FileOutputStream(pictureDir);
                                    fos.write(data);
                                    fos.close();
                                } catch(Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            // Start intent to upload photo
                            UploadImageService.startActionDistressUpload(DistressCameraService.this, eventid, mCurrentPhotoPath);

                            camera.release();
                            pictureCounter++;

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Starting to take 2nd photo
                                    Camera camera = openFrontFacingCamera();

                                    try {
                                        camera.setPreviewDisplay(holder);
                                    } catch (IOException e) {
                                        throw new RuntimeException(e);
                                    }

                                    camera.startPreview();

                                    Camera.Parameters parameters = camera.getParameters();
                                    parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
                                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

                                    camera.setParameters(parameters);

                                    camera.takePicture(null, null, new Camera.PictureCallback() {
                                        @Override
                                        public void onPictureTaken(byte[] data, Camera camera) {
                                            // Create new photo file directory
                                            File pictureDir = null;

                                            try {
                                                pictureDir = createImageFile();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }

                                            // Save to the directory if file is created
                                            if (pictureDir != null) {
                                                try{
                                                    FileOutputStream fos = new FileOutputStream(pictureDir);
                                                    fos.write(data);
                                                    fos.close();
                                                } catch(Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            // Start intent to upload photo
                                            UploadImageService.startActionDistressUpload(DistressCameraService.this, eventid, mCurrentPhotoPath);

                                            camera.release();
                                            pictureCounter++;

                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    // Start taking 3rd photo (front-facing photo)
                                                    Camera camera = Camera.open();

                                                    try {
                                                        camera.setPreviewDisplay(holder);
                                                    } catch (IOException e) {
                                                        throw new RuntimeException(e);
                                                    }

                                                    camera.startPreview();

                                                    Camera.Parameters parameters = camera.getParameters();
                                                    parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
                                                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                                                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);

                                                    camera.setParameters(parameters);

                                                    camera.takePicture(null, null, new Camera.PictureCallback() {
                                                        @Override
                                                        public void onPictureTaken(byte[] data, Camera camera) {
                                                            // Create new photo file directory
                                                            File pictureDir = null;

                                                            try {
                                                                pictureDir = createImageFile();
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }

                                                            // Save to the directory if file is created
                                                            if (pictureDir != null) {
                                                                try {
                                                                    FileOutputStream fos = new FileOutputStream(pictureDir);
                                                                    fos.write(data);
                                                                    fos.close();
                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }

                                                            // Start intent to upload photo
                                                            UploadImageService.startActionDistressUpload(DistressCameraService.this, eventid, mCurrentPhotoPath);

                                                            camera.release();
                                                            pictureCounter++;

                                                            new Handler().postDelayed(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    // Start taking 4th photo (front-facing photo)
                                                                    Camera camera = openFrontFacingCamera();

                                                                    try {
                                                                        camera.setPreviewDisplay(holder);
                                                                    } catch (IOException e) {
                                                                        throw new RuntimeException(e);
                                                                    }

                                                                    camera.startPreview();

                                                                    Camera.Parameters parameters = camera.getParameters();
                                                                    parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
                                                                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

                                                                    camera.setParameters(parameters);

                                                                    camera.takePicture(null, null, new Camera.PictureCallback() {
                                                                        @Override
                                                                        public void onPictureTaken(byte[] data, Camera camera) {
                                                                            // Create new photo file directory
                                                                            File pictureDir = null;

                                                                            try {
                                                                                pictureDir = createImageFile();
                                                                            } catch (IOException e) {
                                                                                e.printStackTrace();
                                                                            }

                                                                            // Save to the directory if file is created
                                                                            if (pictureDir != null) {
                                                                                try {
                                                                                    FileOutputStream fos = new FileOutputStream(pictureDir);
                                                                                    fos.write(data);
                                                                                    fos.close();
                                                                                } catch (Exception e) {
                                                                                    e.printStackTrace();
                                                                                }
                                                                            }

                                                                            // Start intent to upload photo
                                                                            UploadImageService.startActionDistressUpload(DistressCameraService.this, eventid, mCurrentPhotoPath);

                                                                            camera.release();
                                                                            pictureCounter++;

                                                                            stopSelf();
                                                                        }
                                                                    });
                                                                }
                                                            }, cameraDelay);
                                                        }
                                                    });
                                                }
                                            }, cameraDelay);
                                        }
                                    });
                                }
                            }, cameraDelay);
                        }
                    });
                } catch (Exception e) {
                    if (camera != null)
                        camera.release();
                    throw new RuntimeException(e);
                }
            }

            @Override public void surfaceDestroyed(SurfaceHolder holder) {
                holder.removeCallback(this);
            }
            @Override public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}
        });

        if(pictureCounter == 0) {
            windowManager = (WindowManager) context
                    .getSystemService(Context.WINDOW_SERVICE);
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    1, 1, //Must be at least 1x1
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    //Don't know if this is a safe default
                    PixelFormat.TRANSPARENT);

            //Don't set the preview visibility to GONE or INVISIBLE
            windowManager.addView(preview, params);
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "DISTRESS_" + timeStamp + pictureCounter;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private Camera openFrontFacingCamera()
    {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for ( int camIdx = 0; camIdx < cameraCount; camIdx++ ) {
            Camera.getCameraInfo( camIdx, cameraInfo );
            if ( cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT  ) {
                try {
                    cam = Camera.open( camIdx );
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        }

        return cam;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        WindowManager wm = (WindowManager)this
                .getSystemService(Context.WINDOW_SERVICE);
        assert wm != null;
        wm.removeViewImmediate(preview);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
