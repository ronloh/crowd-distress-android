package wenheloh.crowddistress.services;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import id.zelory.compressor.Compressor;
import wenheloh.crowddistress.EventFragment;
import wenheloh.crowddistress.R;
import wenheloh.crowddistress.helpers.SessionManager;

import static wenheloh.crowddistress.helpers.SessionManager.ACTION.ACTION_DISTRESS_UPLOAD;
import static wenheloh.crowddistress.helpers.SessionManager.ACTION.ACTION_PROFILE_UPLOAD;
import static wenheloh.crowddistress.helpers.SessionManager.ACTION.ACTION_EVENT_UPLOAD;
import static wenheloh.crowddistress.helpers.SessionManager.KEY_DOWNLOAD_IMAGEURI;

public class UploadImageService extends IntentService {

    private static final String EXTRA_URI = "wenheloh.crowddistress.helpers.extra.URI";
    private static final String EXTRA_EVENTID = "event_id";
    private static final String EXTRA_EVENTTYPE = "event_type";
    public static final String PROFILE_UPLOAD = "profile_upload_done";
    public static final String EVENT_UPLOAD = "event_upload_done";

    private NotificationManager notificationManager;
    private SessionManager sessionManager;
    private final int notificationId = 100;

    public UploadImageService() {
        super("UploadImageService");
    }

    public static void startActionProfileUpload(Context context, String uri) {
        Intent intent = new Intent(context, UploadImageService.class);
        intent.setAction(ACTION_PROFILE_UPLOAD);
        intent.putExtra(EXTRA_URI, uri);
        context.startService(intent);
    }

    public static void startActionEventUpload(Context context, String eventid, String eventType, String uri) {
        Intent intent = new Intent(context, UploadImageService.class);
        intent.setAction(ACTION_EVENT_UPLOAD);
        intent.putExtra(EXTRA_EVENTID, eventid);
        intent.putExtra(EXTRA_EVENTTYPE, eventType);
        intent.putExtra(EXTRA_URI, uri);
        context.startService(intent);
    }

    public static void startActionDistressUpload(Context context, String eventid, String uri) {
        Intent intent = new Intent(context, UploadImageService.class);
        intent.setAction(ACTION_DISTRESS_UPLOAD);
        intent.putExtra(EXTRA_EVENTID, eventid);
        intent.putExtra(EXTRA_URI, uri);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROFILE_UPLOAD.equals(action)) {
                final String uri = intent.getStringExtra(EXTRA_URI);
                handleActionProfileUpload(uri);
            } else if (ACTION_EVENT_UPLOAD.equals(action)){
                final String eventid = intent.getStringExtra(EXTRA_EVENTID);
                final String eventType = intent.getStringExtra(EXTRA_EVENTTYPE);
                final String uri = intent.getStringExtra(EXTRA_URI);
                handleActionEventUpload(eventid, eventType, uri);
            } else if (ACTION_DISTRESS_UPLOAD.equals(action)) {
                final String eventid = intent.getStringExtra(EXTRA_EVENTID);
                final String uri = intent.getStringExtra(EXTRA_URI);
                handleActionDistressUpload(eventid, uri);
            }
        }
    }

    private void handleActionProfileUpload(String uri) {

        // Start a notification for better UX
        notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = "Crowd Distress";
            String description = "Distress signal";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(SessionManager.CHANNEL_ID.id, name, importance);
            channel.setDescription(description);
            // Register the channel with the system
            notificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, SessionManager.CHANNEL_ID.id)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Uploading profile picture...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setOngoing(true)
                .setCategory(NotificationCompat.CATEGORY_PROGRESS);;

        notificationManager.notify(notificationId, mBuilder.build());

        // Start upload task
        FirebaseStorage storage = FirebaseStorage.getInstance();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String filename = "users/" + currentUser.getUid() + "/" + currentUser.getUid() + ".jpg";
        Uri resultUri = Uri.parse(uri);

        StorageReference storageRef = storage.getReference().child(filename);

        UploadTask uploadTask = storageRef.putFile(resultUri);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                notificationManager.cancel(notificationId);
                Toast.makeText(UploadImageService.this, "Error uploading", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                final Uri downloadUrl = taskSnapshot.getDownloadUrl();

                Intent broadcastIntent = new Intent(PROFILE_UPLOAD);
                broadcastIntent.putExtra("downloadUrl", downloadUrl.toString());
                LocalBroadcastManager.getInstance(UploadImageService.this).sendBroadcast(broadcastIntent);

                SessionManager sessionManager = new SessionManager(UploadImageService.this);
                sessionManager.setDownloadImageUri(downloadUrl.toString());

                DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference()
                        .child("users")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                databaseRef.keepSynced(true);

                HashMap<String, Object> hmUrl = new HashMap<String, Object>();

                hmUrl.put(KEY_DOWNLOAD_IMAGEURI, downloadUrl.toString());

                databaseRef.updateChildren(hmUrl);

                notificationManager.cancel(notificationId);

                Toast.makeText(UploadImageService.this, "Done uploading", Toast.LENGTH_LONG).show();

                stopSelf();
            }
        });
    }

    private void handleActionEventUpload(final String eventId, final String eventType, String uri) {

        // Start a notification for better UX
        notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = "Crowd Distress";
            String description = "Distress signal";
            int importance = NotificationManager.IMPORTANCE_MIN;
            NotificationChannel channel = new NotificationChannel(SessionManager.CHANNEL_ID.id, name, importance);
            channel.setDescription(description);
            // Register the channel with the system
            notificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, SessionManager.CHANNEL_ID.id)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Uploading photo...")
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setOngoing(true)
                .setCategory(NotificationCompat.CATEGORY_PROGRESS);;

        notificationManager.notify(notificationId, mBuilder.build());

        // Start upload task
        FirebaseStorage storage = FirebaseStorage.getInstance();
        String filename = "events/normal/" + eventId + "/" + eventId + ".jpg";
        Uri resultUri = Uri.parse(uri);

        try{
            File imageFile = new File(uri);

            // Start compressing image
            imageFile = new Compressor(this)
                    .setQuality(85)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(imageFile);

            resultUri = Uri.fromFile(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        StorageReference storageRef = storage.getReference().child(filename);

        UploadTask uploadTask = storageRef.putFile(resultUri);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                notificationManager.cancel(notificationId);
                Toast.makeText(UploadImageService.this, "Error uploading", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                sessionManager = new SessionManager(UploadImageService.this);
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

                final Uri downloadUrl = taskSnapshot.getDownloadUrl();

                Intent broadcastIntent = new Intent(EVENT_UPLOAD);
                LocalBroadcastManager.getInstance(UploadImageService.this).sendBroadcast(broadcastIntent);

                DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference()
                        .child("events")
                        .child("normal")
                        .child(eventId);
                databaseRef.keepSynced(true);

                HashMap<String, Object> hmEvent = new HashMap<>();
                hmEvent.put("lat", EventFragment.mLastKnownLocation.getLatitude());
                hmEvent.put("lng", EventFragment.mLastKnownLocation.getLongitude());
                hmEvent.put("timestamp", new Date().getTime());
                hmEvent.put("type", eventType);
                hmEvent.put("username", sessionManager.getName());
                hmEvent.put("uid", currentUser.getUid());

                databaseRef.updateChildren(hmEvent);

                databaseRef.child("imageUrl").push().setValue(downloadUrl.toString());

                notificationManager.cancel(notificationId);

                Toast.makeText(UploadImageService.this, "Report submitted", Toast.LENGTH_LONG).show();

                stopSelf();
            }
        });
    }

    private void handleActionDistressUpload(final String eventId, String uri) {

        // Start upload task in background
        FirebaseStorage storage = FirebaseStorage.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_hh_mm_ss_SSSS");
        String timestamp = formatter.format(new Date().getTime());

        int random = new Random().nextInt(1000);

        String filename = "events/distress/" + eventId + "/" + timestamp + "_" + random + ".jpg";
        Uri resultUri = Uri.parse(uri);

        try{
            File imageFile = new File(uri);

            imageFile = new Compressor(this)
                    .setQuality(70)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(imageFile);

            resultUri = Uri.fromFile(imageFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        StorageReference storageRef = storage.getReference().child(filename);
        UploadTask uploadTask = storageRef.putFile(resultUri);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(UploadImageService.this, "Error uploading", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                sessionManager = new SessionManager(UploadImageService.this);

                final Uri downloadUrl = taskSnapshot.getDownloadUrl();

                final DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference()
                        .child("events")
                        .child("distress");
                databaseRef.keepSynced(true);

                databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.hasChild(eventId)) {
                            databaseRef.child(eventId).child("imageUrl").push().setValue(downloadUrl.toString());
                        } else {
                            DatabaseReference pastDistressRef = FirebaseDatabase.getInstance().getReference()
                                    .child("events")
                                    .child("pastDistress")
                                    .child(eventId);
                            pastDistressRef.keepSynced(true);

                            pastDistressRef.child("imageUrl").push().setValue(downloadUrl.toString());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



                stopSelf();
            }
        });
    }
}
