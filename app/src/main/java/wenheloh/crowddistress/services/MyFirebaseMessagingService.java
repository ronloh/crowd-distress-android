package wenheloh.crowddistress.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.firebase.database.DatabaseReference;

import wenheloh.crowddistress.DistressRingingActivity;
import wenheloh.crowddistress.MainActivity;
import wenheloh.crowddistress.R;
import wenheloh.crowddistress.helpers.SessionManager;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    PowerManager pm;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            final NotificationManager notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // Create the NotificationChannel, but only on API 26+ because
                // the NotificationChannel class is new and not in the support library
                CharSequence name = "Crowd Distress";
                String description = "Distress signal";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel channel = new NotificationChannel(SessionManager.CHANNEL_ID.id, name, importance);
                channel.setDescription(description);
                // Register the channel with the system
                notificationManager.createNotificationChannel(channel);
            }

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, SessionManager.CHANNEL_ID.id)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Received Distress Signal!")
                    .setContentText("Touch to view.")
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setOngoing(true)
                    .setContentIntent(pendingIntent)
                    .setCategory(NotificationCompat.CATEGORY_ALARM);

            notificationManager.notify(DistressRingingActivity.notificationId, mBuilder.build());

            // Call DistressRingingActivity
            Intent distressActivityIntent = new Intent(this, DistressRingingActivity.class);
            startActivity(distressActivityIntent);

            String email = FirebaseAuth.getInstance().getCurrentUser().getEmail().replaceAll("[@.]", "_");
            DatabaseReference isRingingRef = FirebaseDatabase.getInstance().getReference()
                    .child("contacts")
                    .child(email)
                    .child("ringing");

            isRingingRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null)
                        notificationManager.cancel(DistressRingingActivity.notificationId);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
