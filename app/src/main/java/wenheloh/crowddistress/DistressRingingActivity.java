package wenheloh.crowddistress;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.todddavies.components.progressbar.Main;

import wenheloh.crowddistress.model.DistressEvent;

public class DistressRingingActivity extends AppCompatActivity implements OnMapReadyCallback {

    private DistressEvent event;
    private Boolean isAudioOn = true;
    private AudioManager am;
    private MediaPlayer mp;
    public static final int notificationId = 400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distress_ringing);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.setFinishOnTouchOutside(false);
        am =(AudioManager)getSystemService(Context.AUDIO_SERVICE);

        String email = FirebaseAuth.getInstance().getCurrentUser()
                .getEmail().replaceAll("[@.]", "_");

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child("contacts")
                .child(email)
                .child("ringing");

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                event = dataSnapshot.getValue(DistressEvent.class);

                Toolbar toolbar = findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);
                toolbar.setTitle("Distress Signal From " + event.getUsername());

                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(DistressRingingActivity.this);

                // Turn volume to Max
                try{
                    am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
                } catch(NullPointerException e) {
                    e.printStackTrace();
                }

                // Start playing siren sound
                mp = MediaPlayer.create(DistressRingingActivity.this, R.raw.siren);
                mp.setLooping(true);
                mp.start();

                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.btn_openMap:
                                String str_uri = "google.navigation:q=" + event.getLat() + "," + event.getLng();
                                Toast.makeText(DistressRingingActivity.this, str_uri, Toast.LENGTH_SHORT).show();
                                Uri uri = Uri.parse(str_uri);
                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);

                                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(mapIntent);
                                }

                                break;

                            case R.id.btn_mute:

                                if(isAudioOn){
                                    isAudioOn = false;
                                    mp.stop();

                                }else{
                                    isAudioOn = true;
                                    mp = MediaPlayer.create(DistressRingingActivity.this, R.raw.siren);
                                    mp.setLooping(true);
                                    mp.start();
                                }

                                break;

                        }
                    }
                };

                ToggleButton btn_mute = findViewById(R.id.btn_mute);
                btn_mute.setText("Mute");
                btn_mute.setTextOn("Unmute");
                btn_mute.setTextOff("Mute");
                btn_mute.setOnClickListener(listener);

                findViewById(R.id.btn_openMap).setOnClickListener(listener);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Intent intent = new Intent(DistressRingingActivity.this, MainActivity.class);
                startActivity(intent);

                mp.stop();
                mp.release();

                NotificationManager notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
                notificationManager.cancel(notificationId);

                finish();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng center = new LatLng(event.getLat(), event.getLng());
        googleMap.addMarker(new MarkerOptions().position(center));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(center));
        googleMap.setMinZoomPreference(16);
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
