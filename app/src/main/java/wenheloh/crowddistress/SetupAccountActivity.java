package wenheloh.crowddistress;

import android.accounts.Account;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;
import android.widget.Button;

public class SetupAccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_account);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Setup your account");
        setSupportActionBar(toolbar);

        AccountFragment mAccountFragment = new AccountFragment();
        FragmentTransaction mAccountTransaction = getSupportFragmentManager().beginTransaction();
        mAccountTransaction.replace(R.id.frame_setup, mAccountFragment, "SETUP_FRAGMENT");
        mAccountTransaction.commit();
    }
}
